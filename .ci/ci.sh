#!/bin/bash -e

MINGW="$1"
BUILD_TYPE="debug"
[ -z "$MINGW" ] || TARGET='--target=x86_64-pc-windows-gnu'
[[ "$(git describe --all)" = tags/* ]] && OPTIONS='--release' && BUILD_TYPE='release'

rustc --version
cargo --version

echo "Running ${BUILD_TYPE} build ..."

cargo build $TARGET $OPTIONS
[ -z "$MINGW" ] && cargo test $OPTIONS --all --verbose

ls -la ./target/"$BUILD_TYPE"/

if [ -z "$MINGW" ]; then
    echo "Building tarball ..."
    tar czf "edlaunch-rs-$(git describe --tags --always).tar.gz" -C ./target/"$BUILD_TYPE"/ launcher-rs
else
    echo "Building zip ..."
    SRCDIR="$PWD"
    cd ./target/x86_64-pc-windows-gnu/"$BUILD_TYPE"/
    7z -tzip a -mx7 "$SRCDIR/edlaunch-rs-$(git describe --tags --always).zip" launcher-rs.exe
    cd ..
fi

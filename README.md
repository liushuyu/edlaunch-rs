## EDLaunch-rs

A third-party implementation of the official Elite: Dangerous (TM) launcher.

### Building requirements

- `rustc` `cargo`
- `wine` (on Unix-like)
- `gcc` (on Unix-like/Windows) or `MSVC` (Windows)

### Building

`cargo build` for a debug build, `cargo build --release` for a release build.

#### Optional Features

- `secure-password`(enabled by default, use `--no-default-features` to disable): requires `libsecret` `dbus` on Unix-like systems
- `secure-password-kwallet`: requires `dbus` and can only function with KDE KWallet service

### Usage

- On Unix-like OS (Linux/macOS/...) make sure you have wine installed, it's recommended to have [DXVK](https://github.com/doitsujin/dxvk/) installed to your preferred `WINEPREFIX` and do `export WINEPREFIX=<path/to/your/preferred/prefix>`
- Run the launcher, and enter the native path to the directory you want to install the game to (or the directory already contains the game)
- Follow on-screen instructions

### Note

By default, the launcher will use aria2 downloader if available. If you don't want to use aria2, please set `NO_ARIA2` environmental variable to any value.

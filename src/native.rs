#[cfg(windows)]
pub mod platform {
    use crate::networking::EdapiError;
    use anyhow::{anyhow, Result};
    use std::convert::TryInto;
    use std::ffi::{CStr, CString};
    use std::path::Path;
    use std::process::Command;
    use uuid::Uuid;
    use winapi::shared::minwindef::{HKEY, PHKEY};
    use winapi::shared::winerror::ERROR_SUCCESS;
    use winapi::um::winnt::{KEY_READ, KEY_WRITE, REG_OPTION_NON_VOLATILE, REG_SZ};
    use winapi::um::winreg::{
        RegCreateKeyExA, RegOpenCurrentUser, RegOpenKeyExA, RegQueryValueExA, RegSetValueExA,
        HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE,
    };

    pub struct Native {}

    impl Native {
        pub fn new(_prefix: String) -> Result<Native> {
            Ok(Native {})
        }
        fn get_guid(top: HKEY, folder: &str) -> Result<String> {
            let mut key: HKEY = std::ptr::null_mut();
            let mut len: u32 = 128;
            let mut result: Vec<u8> = Vec::with_capacity(len.try_into().unwrap());
            result.reserve(128);
            let folder_cstr = CString::new(folder)?;
            unsafe {
                let len_ptr: *mut u32 = &mut len;
                let hr = RegOpenKeyExA(top, folder_cstr.as_ptr(), 0, KEY_READ, &mut key);
                if hr != 0 {
                    return Err(anyhow!("Error when accessing the registry {}: {}", folder, hr));
                }
                let hr = RegQueryValueExA(
                    key,
                    CString::new("MachineGuid")?.as_ptr(),
                    std::ptr::null_mut(),
                    std::ptr::null_mut(),
                    result.as_mut_ptr(),
                    len_ptr,
                );
                if hr != 0 {
                    return Err(anyhow!("Error when accessing the registry {}: {}", folder, hr));
                }
                result.set_len(len.try_into().unwrap());
            }
            Ok(CStr::from_bytes_with_nul(&result)?
                .to_string_lossy()
                .to_string())
        }
        fn generate_guid() -> Result<String> {
            const FOLDER: &str = "SOFTWARE\\Frontier Developments\\Cryptography";
            let guid = Uuid::new_v4().to_string();
            let guid_str: &str = &guid;
            let guid_ptr = CString::new(guid_str)?;
            let folder_cstr = CString::new(FOLDER)?;
            let mut key: HKEY = std::ptr::null_mut();
            unsafe {
                let hr = RegOpenCurrentUser(KEY_WRITE, &mut key);
                if hr != 0 {
                    return Err(anyhow!("Error when accessing the registry {}: {}", FOLDER, hr));
                }
                let hr = RegCreateKeyExA(
                    key,
                    folder_cstr.as_ptr(),
                    0,
                    std::ptr::null_mut(),
                    REG_OPTION_NON_VOLATILE,
                    KEY_WRITE,
                    std::ptr::null_mut(),
                    &mut key,
                    std::ptr::null_mut(),
                );
                if hr != 0 {
                    return Err(anyhow!("Error when accessing the registry {}: {}", FOLDER, hr));
                }
                let hr = RegSetValueExA(
                    key,
                    CString::new("MachineGuid")?.as_ptr(),
                    0,
                    REG_SZ,
                    guid_ptr.as_ptr() as *const u8,
                    guid_str.len().try_into()?,
                );
                if hr != 0 {
                    return Err(anyhow!("Error when accessing the registry {}: {}", FOLDER, hr));
                }
            }

            Ok(guid)
        }
        pub fn get_machine_id(&self) -> Result<String> {
            let mut guid = String::new();
            let result = Native::get_guid(HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Cryptography")?;
            guid.push_str(&result);
            let result = Native::get_guid(
                HKEY_CURRENT_USER,
                "SOFTWARE\\Frontier Developments\\Cryptography",
            );
            if let Ok(result) = result {
                guid.push_str(&result);
            } else {
                let result = Native::generate_guid()?;
                guid.push_str(&result);
            }

            Ok(guid)
        }
        pub fn get_dos_path<P: AsRef<Path>>(&self, path: P) -> Result<String> {
            Ok(path.as_ref().to_string_lossy().to_string())
        }
        pub fn execute<P: AsRef<Path>>(&self, command: Vec<String>, cwd: P) -> Result<()> {
            let exe = &command[0];
            Command::new(exe)
                .args(&command[1..])
                .current_dir(cwd)
                .spawn()?;
            Ok(())
        }
    }
}

#[cfg(not(windows))]
pub mod platform {
    use anyhow::{anyhow, Result};
    use pest::Parser;
    use std::collections::HashMap;
    use std::path::Path;
    use std::process::Command;
    use uuid::Uuid;
    use winepath::WineConfig;

    #[derive(Parser)]
    #[grammar = "wineregistry.pest"]
    pub struct RegParser;

    pub struct WineRegHive {
        properties: HashMap<String, HashMap<String, String>>,
    }

    impl WineRegHive {
        pub fn new<P: AsRef<Path>>(file: P) -> Result<WineRegHive> {
            let f = std::fs::read_to_string(file)?;
            let parsed = RegParser::parse(Rule::file, &f)?.next().unwrap();
            let mut current_section_name = "";
            let mut properties: HashMap<String, HashMap<String, String>> = HashMap::new();

            for line in parsed.into_inner() {
                match line.as_rule() {
                    Rule::section => {
                        let mut inner_rules = line.into_inner(); // { name }
                        current_section_name = inner_rules.next().unwrap().as_str();
                    }
                    Rule::property => {
                        let mut inner_rules = line.into_inner(); // { name ~ "=" ~ value }
                        let name: String = inner_rules.next().unwrap().as_str().to_string();
                        let value: String = inner_rules.next().unwrap().as_str().to_string();
                        // Insert an empty inner hash map if the outer hash map hasn't
                        // seen this section name before.
                        let section = properties
                            .entry(current_section_name.to_string())
                            .or_default();
                        section.insert(name, value);
                    }
                    Rule::EOI => (),
                    _ => unreachable!(),
                }
            }

            Ok(WineRegHive { properties })
        }

        pub fn get_section(&self, section: &str) -> Option<&HashMap<String, String>> {
            self.properties.get(section)
        }
    }

    pub struct Native {
        prefix: String,
        path_converter: WineConfig,
        sys_registry: WineRegHive,
        user_registry: WineRegHive,
    }

    impl Native {
        pub fn new(prefix: String) -> Result<Native> {
            // spin up Wine
            Command::new("wineboot")
                .env("WINEPREFIX", &prefix)
                .output()?;
            let sys_registry = WineRegHive::new(format!("{}/system.reg", prefix))?;
            let user_registry = WineRegHive::new(format!("{}/user.reg", prefix))?;
            let path_converter = WineConfig::from_prefix(&prefix);

            Ok(Native {
                prefix,
                path_converter,
                sys_registry,
                user_registry,
            })
        }

        fn generate_guid(&self) -> Result<String> {
            let guid = Uuid::new_v4().to_string();
            Command::new("wine")
                .args(&[
                    "reg",
                    "ADD",
                    "HKCU\\Software\\Frontier Developments\\Cryptography",
                    "/v",
                    "MachineGuid",
                    "/t",
                    "REG_SZ",
                    "/d",
                    &guid,
                ])
                .env("WINEPREFIX", self.prefix.clone())
                .output()?;
            Ok(guid)
        }

        pub fn get_machine_id(&mut self) -> Result<String> {
            let mut final_machine_id = String::new();
            let result = self
                .sys_registry
                .get_section(r#"Software\\Microsoft\\Cryptography"#);
            if let Some(result) = result {
                final_machine_id.push_str(
                    result
                        .get(r#""MachineGuid""#)
                        .ok_or_else(|| anyhow!("Could not read machine guid key from the registry"))?
                        .trim_matches(|c| c == '\"'),
                );
            }
            let result = self
                .user_registry
                .get_section(r#"Software\\Frontier Developments\\Cryptography"#);
            if let Some(result) = result {
                let guid = result.get(r#""MachineGuid""#);
                if let Some(guid) = guid {
                    final_machine_id.push_str(guid.trim_matches(|c| c == '\"'));
                } else {
                    final_machine_id.push_str(&self.generate_guid()?);
                }
            } else {
                final_machine_id.push_str(&self.generate_guid()?);
            }

            Ok(final_machine_id)
        }
        pub fn get_dos_path<P: AsRef<Path>>(&mut self, path: P) -> Result<String> {
            let result = self.path_converter.to_wine_path(path)?;

            Ok(result.to_string())
        }
        pub fn execute<P: AsRef<Path>>(&self, command: Vec<String>, cwd: P) -> Result<()> {
            Command::new("wine")
                .args(command)
                .env("WINEPREFIX", &self.prefix)
                .current_dir(cwd)
                .spawn()?
                .wait()?;
            Ok(())
        }
    }
}

use anyhow::Result;
use quick_xml;
use quick_xml::events::Event;
use serde::Deserialize;
use serde_json;
use std::io::Write;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::thread;

use crate::utils::{hash_file_sha1, EdLaunchArgs};

#[derive(Deserialize, Debug)]
pub struct EdapiProductMeta {
    #[serde(rename = "Version")]
    pub version: String,
    pub executable: String,
    pub name: String,
    #[serde(rename = "useWatchDog64")]
    #[serde(default)]
    pub use_watch_dog64: String,
    #[serde(skip)]
    sku: String,
    #[serde(default)]
    pub offline: String,
}

#[derive(Debug)]
pub struct EdapiAsset {
    pub path: String,
    pub url: String,
    hash: String,
    size: usize,
}

impl EdapiProductMeta {
    pub fn new(name: String, sku: String) -> EdapiProductMeta {
        EdapiProductMeta {
            version: String::new(),
            executable: String::new(),
            name,
            sku,
            offline: String::new(),
            use_watch_dog64: String::new(),
        }
    }
}

impl EdapiAsset {
    pub fn new() -> EdapiAsset {
        EdapiAsset {
            path: String::new(),
            url: String::new(),
            hash: String::new(),
            size: 0,
        }
    }
}

pub struct EdapiFS {
    root: String,
}

impl EdapiFS {
    pub fn new(root: String) -> EdapiFS {
        EdapiFS { root }
    }

    pub fn fill_launch_args(&self, meta: &EdapiProductMeta, args: &mut EdLaunchArgs) -> Result<()> {
        args.main_exe_path = self.get_main_exe_path(meta)?.to_string_lossy().into_owned();
        args.main_exe_version = meta.version.clone();
        args.watch_dog_path = self
            .get_watch_dog_path(meta)?
            .to_string_lossy()
            .into_owned();

        Ok(())
    }

    pub fn get_watch_dog_path(&self, meta: &EdapiProductMeta) -> Result<PathBuf> {
        let mut path = PathBuf::from(self.root.clone());
        if meta.use_watch_dog64 == "true" {
            path.push("WatchDog64.exe");
        } else {
            path.push("WatchDog.exe");
        }
        Ok(path.canonicalize()?)
    }

    pub fn get_main_exe_path(&self, meta: &EdapiProductMeta) -> Result<PathBuf> {
        let mut path = PathBuf::from(self.root.clone());
        path.push("Products");
        path.push(meta.sku.clone());
        path.push(meta.executable.clone());
        Ok(path.canonicalize()?)
    }

    pub fn get_product_path(&self, meta: &EdapiProductMeta) -> Result<PathBuf> {
        let mut path = PathBuf::from(self.root.clone());
        path.push("Products");
        path.push(meta.sku.clone());
        if !path.exists() {
            std::fs::create_dir_all(path.clone())?;
        }
        Ok(path.canonicalize()?)
    }

    pub fn read_meta(&self, dir: String) -> Result<EdapiProductMeta> {
        let mut path = PathBuf::from(self.root.clone());
        path.push("Products");
        path.push(dir.clone());
        path.push("VersionInfo.txt");

        let f = std::fs::File::open(path)?;
        let mut meta: EdapiProductMeta = serde_json::from_reader(f)?;
        meta.sku = dir;
        Ok(meta)
    }

    pub fn verify_files(&self, meta: &EdapiProductMeta, files: Vec<EdapiAsset>) -> Vec<EdapiAsset> {
        let files = Arc::from(Mutex::new(files));
        let mut to_process: Vec<EdapiAsset> = Vec::new();
        to_process.reserve(36000);
        let to_process = Arc::new(Mutex::new(to_process));
        let mut handles = vec![];
        // calculate the path to the product directory
        let mut path = PathBuf::from(self.root.clone());
        path.push("Products");
        path.push(meta.sku.clone());

        for _ in 0..2 {
            let files = Arc::clone(&files);
            let root = path.clone();
            let to_process = Arc::clone(&to_process);
            let handle = thread::spawn(move || loop {
                let file = files.lock().unwrap().pop();
                let root_copy = root.clone();
                if file.is_none() {
                    break;
                }
                let file_copy = file.unwrap();
                if verify_file(&file_copy, root_copy) {
                    continue;
                }
                to_process.lock().unwrap().push(file_copy);
            });

            handles.push(handle);
        }

        for handle in handles {
            handle.join().unwrap();
        }

        // consumes the Arc and Mutex to release their contents
        Arc::try_unwrap(to_process).unwrap().into_inner().unwrap()
    }
}

#[derive(Clone, Copy)]
enum State {
    None,
    Path,
    URL,
    Hash,
    Size,
}

#[inline]
pub fn total_size(files: &[EdapiAsset]) -> usize {
    files.iter().map(|f| f.size).sum()
}

fn verify_file(asset: &EdapiAsset, mut root: PathBuf) -> bool {
    let components: PathBuf = asset.path.split('\\').collect();
    root.push(components);
    if let Some(hash) = hash_file_sha1(root) {
        return hash == asset.hash;
    }
    false
}

pub fn get_aria2_control_file(assets: &[EdapiAsset], root: &PathBuf) -> Result<Vec<u8>> {
    let mut buffer = Vec::with_capacity(assets.len() * 1024);
    let path = root.to_string_lossy();
    for asset in assets {
        write!(
            &mut buffer,
            "{}\n dir={}\n out={}\n checksum=sha-1={}\n allow-overwrite=true\n",
            asset.url,
            path,
            asset.path.replace("\\", "/"),
            asset.hash
        )?;
    }
    Ok(buffer)
}

pub fn get_file_list(xml: Vec<u8>) -> Result<Vec<EdapiAsset>> {
    let mut xml_reader = quick_xml::Reader::from_reader(xml.as_slice());
    let mut buf = Vec::new();
    let mut files: Vec<EdapiAsset> = Vec::new();
    let mut state = State::None;
    let mut file: EdapiAsset = EdapiAsset::new();
    files.reserve(36000);

    loop {
        match xml_reader.read_event(&mut buf) {
            Ok(Event::Start(ref e)) => match e.name() {
                b"Path" => {
                    state = State::Path;
                }
                b"Size" => {
                    state = State::Size;
                }
                b"Download" => {
                    state = State::URL;
                }
                b"Hash" => {
                    state = State::Hash;
                }
                _ => {
                    state = State::None;
                }
            },
            Ok(Event::End(ref e)) => {
                if e.name() == b"File" {
                    files.push(file);
                    file = EdapiAsset::new();
                    state = State::None;
                }
            }
            Ok(Event::Text(e)) => {
                match state {
                    State::None => (),
                    State::URL => file.url = e.unescape_and_decode(&xml_reader)?,
                    State::Size => file.size = e.unescape_and_decode(&xml_reader)?.parse()?,
                    State::Path => file.path = e.unescape_and_decode(&xml_reader)?,
                    State::Hash => file.hash = e.unescape_and_decode(&xml_reader)?,
                }
                state = State::None;
            }
            Ok(Event::Eof) => break,
            _ => (),
        }
    }

    buf.clear();
    Ok(files)
}

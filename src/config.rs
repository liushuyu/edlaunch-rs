use crate::networking::EdapiConfig;
use anyhow::Result;
use bincode;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    api_config: EdapiConfig,
    wine_prefix: String,
    game_prefix: String,
}

impl Config {
    pub fn new(api_config: EdapiConfig) -> Config {
        Config {
            api_config,
            wine_prefix: String::new(),
            game_prefix: String::new(),
        }
    }
    pub fn load(input: Vec<u8>) -> Result<Config> {
        let config: Config = bincode::deserialize(&input)?;
        Ok(config)
    }
    pub fn save(&self) -> Result<Vec<u8>> {
        Ok(bincode::serialize(&self)?)
    }
    pub fn get_api_config(&self) -> &EdapiConfig {
        &self.api_config
    }
    pub fn get_wine_prefix(&self) -> String {
        self.wine_prefix.clone()
    }
    pub fn get_game_prefix(&self) -> String {
        self.game_prefix.clone()
    }
    pub fn set_game_config(&mut self, game_prefix: String, wine_prefix: String) {
        self.game_prefix = game_prefix;
        self.wine_prefix = wine_prefix;
    }
}

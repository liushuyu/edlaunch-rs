use anyhow::{anyhow, Result};
use sha1::{Digest, Sha1};
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::process::Command;
use tempfile::NamedTempFile;

#[derive(Debug)]
pub struct EdLaunchArgs {
    pub watch_dog_path: String,
    pub main_exe_path: String,
    pub main_exe_args: String,
    pub auth_token: String,
    pub machine_id: String,
    pub machine_token: String,
    pub main_exe_version: String,
}

pub fn ascii_encode(s: &str) -> String {
    hex::encode_upper(s)
}

pub fn ascii_decode(s: &str) -> Result<Vec<u8>, hex::FromHexError> {
    hex::decode(s)
}

pub fn ftime() -> String {
    chrono::Utc::now().format("%s%.6f").to_string()[..16].to_string()
}

pub fn check_time(reference: i64) -> Result<i64> {
    Ok(chrono::Utc::now().timestamp() - reference)
}

pub fn hash_file_sha1<P: AsRef<Path>>(file: P) -> Option<String> {
    let mut hasher = Sha1::new();
    if let Ok(mut file) = File::open(file) {
        if std::io::copy(&mut file, &mut hasher).is_err() {
            return None;
        }
        return Some(format!("{:x}", hasher.finalize()));
    }
    None
}

pub fn machine_id(guid: &str) -> String {
    format!("{:x}", Sha1::new().chain(guid).finalize())
}

pub fn make_installer_args(exe: &str, path: &str) -> Vec<String> {
    vec![
        exe.to_string(),
        "/SP-".to_string(),
        "/VERYSILENT".to_string(),
        "/NOCANCEL".to_string(),
        "/NOICONS".to_string(),
        "/NORESTART".to_string(),
        // "/SUPPRESSMSGBOXES".to_string(),
        format!("/DIR={}", path),
    ]
}

pub fn make_launch_args<F: FnMut(String) -> String>(
    args: EdLaunchArgs,
    convert: &mut Option<F>,
) -> Option<Vec<String>> {
    let exe_hash = hash_file_sha1(args.main_exe_path.clone());
    let mut watch_dog_path = args.watch_dog_path;
    let mut main_exe_path = args.main_exe_path;
    exe_hash.as_ref()?;
    if let Some(convert) = convert {
        watch_dog_path = convert(watch_dog_path);
        main_exe_path = convert(main_exe_path);
    }
    Some(vec![
        watch_dog_path,
        "/Executable".to_string(),
        main_exe_path,
        "/ExecutableArgs".to_string(),
        format!(
            r#" "ServerToken {} {} {}" /language English\\UK"#,
            args.machine_token, args.auth_token, args.main_exe_args
        ),
        "/novr".to_string(),
        "/MachineToken".to_string(),
        args.machine_token,
        "/Version".to_string(),
        args.main_exe_version,
        "/AuthToken".to_string(),
        args.auth_token.clone(),
        "/MachineId".to_string(),
        args.machine_id,
        "/Time".to_string(),
        ftime(),
        "/ExecutableHash".to_string(),
        exe_hash.unwrap().to_uppercase(),
    ])
}

pub fn has_aria2() -> bool {
    if std::env::var("NO_ARIA2").is_ok() {
        return false;
    }
    let output = Command::new("aria2c").arg("-v").output();
    if let Ok(output) = output {
        return output.status.success();
    }

    false
}

pub fn aria2_download(buffer: &[u8]) -> Result<()> {
    let mut list_file = NamedTempFile::new()?;
    list_file.write_all(buffer)?;
    let (_, list_file_path) = list_file.keep()?;
    let child = Command::new("aria2c")
        .arg("-V")
        .arg("-i")
        .arg(list_file_path.clone())
        .spawn()?
        .wait()?;
    std::fs::remove_file(&list_file_path).ok();
    if !child.success() {
        return Err(anyhow!("Download was not successful"));
    }

    Ok(())
}

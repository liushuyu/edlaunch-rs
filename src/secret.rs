#[cfg(feature = "secure-password-kwallet")]
use dbus::blocking::Connection;
#[cfg(feature = "secure-password-kwallet")]
use failure::Error;
#[cfg(feature = "secure-password")]
use keyring::Keyring;
#[cfg(feature = "secure-password-kwallet")]
use std::time::Duration;

const APP_NAME: &str = "edlaunch-rs";

#[cfg(feature = "secure-password-kwallet")]
fn get_password_kwallet(username: &str) -> Result<String, Error> {
    let conn = Connection::new_session()?;
    let mut proxy = conn.with_proxy(
        "org.kde.kwalletd5",
        "/modules/kwalletd5",
        Duration::from_millis(3000),
    );
    proxy.timeout = Duration::from_secs(45);
    let id: i64 = 0;
    let wallet_id: (i32,) =
        proxy.method_call("org.kde.KWallet", "open", ("kdewallet", id, APP_NAME))?;
    let wallet_id = wallet_id.0;
    // Signature:
    // method QString org.kde.KWallet.readPassword(int handle, QString folder, QString key, QString appid)
    let password: (String,) = proxy.method_call(
        "org.kde.KWallet",
        "readPassword",
        (wallet_id, APP_NAME, username, APP_NAME),
    )?;
    Ok(password.0)
}

#[cfg(feature = "secure-password-kwallet")]
fn set_password_kwallet(username: &str, password: &str) -> Result<i32, Error> {
    let conn = Connection::new_session()?;
    let mut proxy = conn.with_proxy(
        "org.kde.kwalletd5",
        "/modules/kwalletd5",
        Duration::from_millis(3000),
    );
    proxy.timeout = Duration::from_secs(30);
    let id: i64 = 0;
    let wallet_id: (i32,) =
        proxy.method_call("org.kde.KWallet", "open", ("kdewallet", id, APP_NAME))?;
    let wallet_id = wallet_id.0;
    // Signature:
    // method int org.kde.KWallet.writePassword(int handle, QString folder, QString key, QString value, QString appid)
    let result: (i32,) = proxy.method_call(
        "org.kde.KWallet",
        "writePassword",
        (wallet_id, APP_NAME, username, password, APP_NAME),
    )?;

    Ok(result.0)
}

#[cfg(feature = "secure-password-kwallet")]
fn erase_password_kwallet(username: &str) -> Result<i32, Error> {
    let conn = Connection::new_session()?;
    let mut proxy = conn.with_proxy(
        "org.kde.kwalletd5",
        "/modules/kwalletd5",
        Duration::from_millis(3000),
    );
    proxy.timeout = Duration::from_secs(30);
    let id: i64 = 0;
    let wallet_id: (i32,) =
        proxy.method_call("org.kde.KWallet", "open", ("kdewallet", id, APP_NAME))?;
    let wallet_id = wallet_id.0;
    // method int org.kde.KWallet.removeEntry(int handle, QString folder, QString key, QString appid)
    let result: (i32,) = proxy.method_call(
        "org.kde.KWallet",
        "removeEntry",
        (wallet_id, APP_NAME, username, APP_NAME),
    )?;

    Ok(result.0)
}

pub struct Secret {
    kwallet: bool,
}

impl Secret {
    pub fn new(kwallet: bool) -> Secret {
        Secret { kwallet }
    }

    pub fn get(&self, username: &str) -> Option<String> {
        let mut result: Option<String> = None;
        #[cfg(feature = "secure-password-kwallet")]
        {
            if self.kwallet {
                match get_password_kwallet(username) {
                    Ok(password) => result = Some(password),
                    Err(_) => result = None,
                }
                return result;
            }
        }
        #[cfg(feature = "secure-password")]
        {
            let cred_store = Keyring::new(APP_NAME, username);
            match cred_store.get_password() {
                Ok(password) => result = Some(password),
                Err(_) => result = None,
            }
        }

        result
    }

    pub fn set(&self, username: &str, password: &str) -> bool {
        let mut result: bool = false;
        #[cfg(feature = "secure-password-kwallet")]
        {
            if self.kwallet {
                match set_password_kwallet(username, password) {
                    Ok(_) => result = true,
                    Err(_) => result = false,
                }
                return result;
            }
        }
        #[cfg(feature = "secure-password")]
        {
            let cred_store = Keyring::new(APP_NAME, username);
            match cred_store.set_password(password) {
                Ok(_) => result = true,
                Err(_) => result = false,
            }
        }

        result
    }

    pub fn erase(&self, username: &str) -> bool {
        let mut result: bool = false;
        #[cfg(feature = "secure-password-kwallet")]
        {
            if self.kwallet {
                match erase_password_kwallet(username) {
                    Ok(_) => result = true,
                    Err(_) => result = false,
                }
                return result;
            }
        }
        #[cfg(feature = "secure-password")]
        {
            let cred_store = Keyring::new(APP_NAME, username);
            match cred_store.delete_password() {
                Ok(_) => result = true,
                Err(_) => result = false,
            }
        }

        result
    }
}

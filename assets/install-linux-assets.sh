#!/bin/bash -ex
PREFIX="${PREFIX:-/usr/local/}"
SRCROOT="$(dirname "$0")"

install -dv "${PREFIX}/share/applications/"
install -dv "${PREFIX}/share/pixmaps/"
install -Dvm644 "$SRCROOT/edlaunch-rs.desktop" "$PREFIX/share/applications/"
if ! command -v svgo; then
    echo 'WARNING: svgo is not installed. Installing the original SVG file ...'
    install -Dvm644 "$SRCROOT/../icon.svg" "$PREFIX/share/pixmaps/edlaunch-rs.svg"
else
    echo 'INFO: Trimming SVG file ...'
    svgo -i "$SRCROOT/../icon.svg" -o "$PREFIX/share/pixmaps/edlaunch-rs.svg"
fi
if command -v inkscape; then
    echo 'INFO: Converting SVG to PNG (Inkscape) ...'
    inkscape --export-type=png --export-dpi=200 --export-background-opacity=0 "$SRCROOT/../icon.svg"
    mv -v "$SRCROOT/../icon.png" "$PREFIX/share/pixmaps/edlaunch-rs.png"
elif command -v convert; then
    echo 'INFO: Converting SVG to PNG (ImageMagick, lower quality) ...'
    convert "$SRCROOT/../icon.svg" -background none -transparent white -quality 100 "$PREFIX/share/pixmaps/edlaunch-rs.png"
else
    echo 'WARNING: Neither ImageMagick nor Inkscape is installed. PNG icon will not be installed.'
fi

echo 'Done'

#[cfg(not(windows))]
#[macro_use]
extern crate pest_derive;

mod config;
mod maintenance;
mod native;
mod networking;
mod secret;
mod utils;

use anyhow::Result;
use config::Config;
use console::style;
use dialoguer::{theme, Input, Password, Select};
use dirs;
use indicatif;
use lazy_static::lazy_static;
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::thread;
use tokio::runtime::Runtime;

struct GameConfig {
    game_prefix: String,
    wine_prefix: String,
}

const INSTALLER_NAME: &str = "elite-installer.exe";

lazy_static! {
    static ref SPINNER_STYLE: indicatif::ProgressStyle =
        indicatif::ProgressStyle::default_spinner()
            .tick_chars("⠋⠙⠸⠴⠦⠇ ")
            .template("{spinner:.green} {wide_msg}");
}

fn ask_captcha() -> String {
    Input::<String>::new()
        .with_prompt("Verification code from email")
        .interact()
        .unwrap()
}

fn save_config(api: &networking::EDAPI, cfg: &GameConfig) -> Result<()> {
    let path = dirs::config_dir().unwrap_or_else(|| PathBuf::from("./"));
    let mut api_config = path;
    api_config.push("edlaunch_config");
    let mut config = Config::new(api.serialize_config());
    config.set_game_config(cfg.game_prefix.clone(), cfg.wine_prefix.clone());
    let config_data = config.save()?;
    let mut f = File::create(api_config)?;
    f.write_all(&config_data)?;

    Ok(())
}

fn load_config(api: &mut networking::EDAPI, cfg: &mut GameConfig) -> Result<()> {
    let path = dirs::config_dir().unwrap_or_else(|| PathBuf::from("./"));
    let mut api_config = path;
    api_config.push("edlaunch_config");
    let mut data: Vec<u8> = Vec::new();
    let mut f = File::open(api_config)?;
    f.read_to_end(&mut data)?;
    let config = Config::load(data)?;
    api.deseralize_config(config.get_api_config());
    cfg.game_prefix = config.get_game_prefix();
    cfg.wine_prefix = config.get_wine_prefix();

    Ok(())
}

fn pretty_print_products(products: &[networking::EdapiProduct]) -> Vec<String> {
    let flattened: Vec<String> = products
        .iter()
        .map(|product| product.product_name.clone())
        .collect();

    flattened
}

async fn prepare_to_update(
    api: &mut networking::EDAPI,
    meta: &networking::EdapiAssetMeta,
    fs_meta: &maintenance::EdapiProductMeta,
    fs: &maintenance::EdapiFS,
) -> Vec<maintenance::EdapiAsset> {
    println!("Fetching metadata...");
    let manifest = api.fetch_manifest(meta).await.unwrap();
    let files = maintenance::get_file_list(manifest.to_vec()).unwrap();
    println!("Verifying files...");
    let files_to_download = fs.verify_files(fs_meta, files);
    println!("{} files to download", files_to_download.len());

    files_to_download
}

#[inline]
fn create_spinner(msg: &str, tick_rate: u64) -> indicatif::ProgressBar {
    let spinner = indicatif::ProgressBar::new_spinner().with_style(SPINNER_STYLE.clone());
    spinner.set_message(msg.to_owned());
    spinner.enable_steady_tick(tick_rate);

    spinner
}

async fn download_update(
    fs_meta: &maintenance::EdapiProductMeta,
    fs: &maintenance::EdapiFS,
    files: Vec<maintenance::EdapiAsset>,
) {
    println!("Calculating changes...");
    let total: u64 = maintenance::total_size(&files).try_into().unwrap();
    println!("Total size: {}", indicatif::HumanBytes(total));
    let progress_bar = indicatif::ProgressBar::new(total);
    progress_bar.set_style(indicatif::ProgressStyle::default_bar().template(
        "{spinner} [{bar:40.cyan/blue}] {bytes}/{total_bytes} ({bytes_per_sec}, eta {eta})",
    ));
    progress_bar.enable_steady_tick(500);
    let path = fs.get_product_path(fs_meta).unwrap();
    let files = Arc::from(Mutex::new(files));
    let progress_bar = Arc::new(Mutex::new(progress_bar));
    let mut handles = vec![];
    for _ in 0..3 {
        let files = Arc::clone(&files);
        let progress_bar = Arc::clone(&progress_bar);
        let path_clone = path.clone();
        let handle = thread::spawn(move || {
            let tokio_rt = Runtime::new().unwrap();
            loop {
                let file = files.lock().unwrap().pop();
                let mut path_clone: PathBuf = path_clone.clone();
                if file.is_none() {
                    break;
                }
                let file_copy = file.unwrap();
                path_clone.push(file_copy.path.replace("\\", "/"));
                let dir = path_clone.parent();
                if let Some(dir) = dir {
                    std::fs::create_dir_all(&dir).unwrap();
                }
                let size = tokio_rt.block_on(networking::download(&file_copy.url, path_clone));
                progress_bar.lock().unwrap().inc(size.unwrap());
            }
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
    }
    progress_bar
        .lock()
        .unwrap()
        .finish_with_message("Download completed");
}

#[tokio::main]
async fn main() -> Result<()> {
    let fs;
    let mut api = networking::EDAPI::new();
    let email;
    let mut pwd = String::new();
    let menu_style = theme::ColorfulTheme::default();
    let mut select_menu = Select::with_theme(&menu_style);
    let mut wine;
    let mut password_saved = false;
    let ss: secret::Secret;
    let mut cfg = GameConfig {
        game_prefix: String::new(),
        wine_prefix: String::new(),
    };
    // end of variable declarations
    load_config(&mut api, &mut cfg).unwrap_or(());
    if !cfg!(windows) && cfg.wine_prefix.is_empty() {
        let prefix = std::env::var("WINEPREFIX").unwrap_or_else(|_| {
            panic!("Please set WINEPREFIX environment variable!");
        });
        cfg.wine_prefix = prefix;
    }
    if cfg.game_prefix.is_empty() {
        let prefix = Input::<String>::with_theme(&menu_style)
            .with_prompt("Unix path to the game directory")
            .interact()?;
        cfg.game_prefix = prefix;
    }
    fs = maintenance::EdapiFS::new(cfg.game_prefix.clone());
    ss = secret::Secret::new(false);
    let spinner = create_spinner("Initializing Wine...", 100);
    wine = native::platform::Native::new(cfg.wine_prefix.clone()).unwrap();
    spinner.set_message("Initializing API...");
    api.probe().await.unwrap();
    spinner.finish_and_clear();
    let mut watch_dog_path = PathBuf::from(cfg.game_prefix.clone());
    watch_dog_path.push("WatchDog64.exe");
    if !watch_dog_path.is_file() {
        let spinner = indicatif::ProgressBar::new_spinner();
        spinner.set_message("Downloading and installing base files...");
        spinner.enable_steady_tick(300);
        let mut installer_path = PathBuf::from(cfg.game_prefix.clone());
        installer_path.push(INSTALLER_NAME);
        networking::download_installer(installer_path.clone()).await?;
        let install_args = utils::make_installer_args(
            &installer_path.to_string_lossy(),
            &wine.get_dos_path(cfg.game_prefix.clone())?,
        );
        wine.execute(install_args, cfg.game_prefix.clone())?;
        spinner.finish_and_clear();
    }
    let guid = wine.get_machine_id()?;
    // if needs setup or the machine ID does not match
    if api.needs_setup() || !api.check_machine_id(guid.clone()) {
        api.set_machine_id(guid);
        email = Input::<String>::with_theme(&menu_style)
            .with_prompt("Email address")
            .interact()?;
        api.set_email(&email);
    } else {
        println!("Email address: {}", api.get_email());
    }
    if api.needs_password() {
        let spinner = create_spinner("Querying system secure storage...", 90);
        let ss_result = ss.get(&api.get_email());
        spinner.finish_and_clear();
        if let Some(password) = ss_result {
            pwd = password;
            println!("Password: {}", style("[Received from keyring]").cyan());
            password_saved = true;
        } else {
            spinner.finish_and_clear();
            pwd = Password::with_theme(&menu_style)
                .with_prompt("Password")
                .interact()?;
        }
    }

    let spinner = create_spinner("Logging In...", 100);
    let login_result = api
        .auth(&pwd, || {
            spinner.disable_steady_tick();
            ask_captcha()
        })
        .await;
    if login_result.is_err() && password_saved {
        ss.erase(&api.get_email());
        println!("Failed to login, saved password has been erased.");
        return Ok(());
    }
    let products = api.get_product_list().await.unwrap();
    save_config(&api, &cfg).unwrap();
    spinner.finish_and_clear();

    if !password_saved
        && dialoguer::Confirm::new()
            .with_prompt("Do you want to save the password?")
            .interact()?
    {
        if !ss.set(&api.get_email(), &pwd) {
            println!("Unable to save your password...");
        } else {
            println!("Password saved successfully.");
        }
    }

    let choice: usize = select_menu
        .items(&pretty_print_products(&products))
        .with_prompt("Choose a mode to launch")
        .default(0)
        .interact()?;
    let chosen = &products[choice];

    let spinner = create_spinner("Checking for updates...", 100);

    let updates = api.check_update(chosen.product_sku.clone()).await.unwrap();
    let mut fs_meta = fs.read_meta(chosen.directory.clone()).unwrap_or_else(|_| {
        maintenance::EdapiProductMeta::new(chosen.product_name.clone(), chosen.directory.clone())
    });

    spinner.finish_and_clear();

    if updates.version != fs_meta.version {
        println!("Updating from {} to {}", fs_meta.version, updates.version);
        let files: Vec<maintenance::EdapiAsset> =
            prepare_to_update(&mut api, &updates, &fs_meta, &fs).await;
        if utils::has_aria2() {
            let control_buffer =
                maintenance::get_aria2_control_file(&files, &fs.get_product_path(&fs_meta)?)?;
            utils::aria2_download(&control_buffer)?;
        } else {
            download_update(&fs_meta, &fs, files).await;
        }
        fs_meta = fs.read_meta(chosen.directory.clone()).unwrap();
    }
    println!("The game is up-to-date!");
    if fs_meta.offline != "true" {
        let mut args = api.make_launch_args(chosen).unwrap();
        fs.fill_launch_args(&fs_meta, &mut args).unwrap();
        // end
        let spinner = create_spinner("Preparing to launch...", 100);
        let baked_args = utils::make_launch_args(
            args,
            &mut Some(&mut |path: String| wine.get_dos_path(path.clone()).unwrap_or(path)),
        )
        .unwrap();
        spinner.finish_and_clear();
        wine.execute(baked_args, fs.get_product_path(&fs_meta)?)?;
    } else {
        println!("Launching...");
        let path = fs.get_main_exe_path(&fs_meta)?;
        let path = wine.get_dos_path(path).unwrap();
        wine.execute(vec![path], fs.get_product_path(&fs_meta)?)?;
    }

    Ok(())
}

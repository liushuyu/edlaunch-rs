use crate::utils;
use anyhow::Result;
use async_compression::tokio::write::GzipDecoder;
use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::Path;
use tokio::{fs::File, io::AsyncWriteExt};

// Macro definitions
macro_rules! API_VER {
    () => {
        "0.4.6568.0"
    };
}

macro_rules! API_URL {
    () => {
        "https://api.zaonce.net/1.1/"
    };
}

// Error definitions
#[derive(Debug)]
pub struct EdapiError {
    brief: String,
    message: Option<String>,
}

impl EdapiError {
    pub fn new(brief: String, message: Option<String>) -> EdapiError {
        EdapiError { brief, message }
    }
}

impl fmt::Display for EdapiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "API Error: {}", self.brief)?;
        if let Some(message) = &self.message {
            write!(f, ": {}", message)?;
        }
        Ok(())
    }
}

impl std::error::Error for EdapiError {}

// JSON prototypes
#[derive(Deserialize)]
struct EdapiVersion {
    #[serde(rename = "versionLatest")]
    version_latest: String,
    #[serde(rename = "versionStatus")]
    version_status: String,
}

#[derive(Deserialize)]
struct EdapiStatus {
    status: i32,
    text: String,
}

#[derive(Deserialize)]
struct EdapiTime {
    #[serde(rename = "unixTimestamp")]
    timestamp: i64,
}

#[derive(Deserialize)]
struct EdapiAuth {
    #[serde(default)]
    #[serde(rename = "registeredName")]
    #[allow(dead_code)] // TODO: should use this field
    registered_name: String,
    #[serde(default)]
    #[serde(rename = "encCode")]
    enc_code: String,
    #[serde(default)]
    #[serde(rename = "machineToken")]
    machine_token: String,
    #[serde(default)]
    #[serde(rename = "authToken")]
    auth_token: String,
}

#[derive(Deserialize, Debug)]
pub struct EdapiProduct {
    pub directory: String,
    pub template: String,
    pub filter: String,
    pub serverargs: String,
    pub gameargs: String,
    pub product_name: String,
    pub product_sku: String,
    pub sortkey: String,
    pub colour: String,
}

#[derive(Deserialize, Debug)]
pub struct EdapiProducts {
    purchases: Vec<EdapiProduct>,
}

#[derive(Deserialize, Debug)]
pub struct EdapiAssetMeta {
    #[serde(rename = "localFile")]
    local_file: String,
    md5: String,
    #[serde(rename = "remotePath")]
    remote_path: String,
    size: String,
    pub version: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EdapiConfig {
    machine_token: String,
    machine_id: String,
    #[serde(default)]
    user: String,
    #[serde(default)]
    secret: String,
}

// constant definitions
const API_VERSION: &str = API_VER!();
const API_USER_AGENT: &str = concat!("EDLaunch/", API_VER!(), "/Win64");
const API_URL_3: &str = "https://api.zaonce.net/3.0/";
const API_URL_STATUS: &str = "http://hosting.zaonce.net/launcher-status/status.json";
const INSTALLER_URL: &str = "http://hosting.zaonce.net/elite/Client-Installer.exe";

// main EDAPI struct and its impl's
#[derive(Debug)]
pub struct EDAPI {
    machine_token: String,
    machine_id: String,
    auth_token: String,
    email: String,
    request: reqwest::Client,
}

impl EDAPI {
    pub fn new() -> EDAPI {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_static(API_USER_AGENT));

        EDAPI {
            machine_id: String::new(),
            machine_token: String::new(),
            auth_token: String::new(),
            email: String::new(),
            request: reqwest::Client::builder()
                .cookie_store(true)
                .default_headers(headers)
                .build()
                .unwrap(),
        }
    }

    pub fn serialize_config(&self) -> EdapiConfig {
        EdapiConfig {
            machine_id: self.machine_id.clone(),
            machine_token: self.machine_token.clone(),
            user: self.email.clone(),
            secret: String::new(),
        }
    }

    pub fn deseralize_config(&mut self, config: &EdapiConfig) {
        self.email = config.user.clone();
        self.machine_token = config.machine_token.clone();
        self.machine_id = config.machine_id.clone();
        self.auth_token = config.secret.clone();
    }

    pub fn make_launch_args(&self, product: &EdapiProduct) -> Result<utils::EdLaunchArgs> {
        let mut main_exe_args = product.gameargs.clone();
        if !product.serverargs.is_empty() {
            if !main_exe_args.is_empty() {
                main_exe_args.push_str(" "); // add a space as separator
            }
            main_exe_args.push_str(&product.serverargs);
        }
        let args = utils::EdLaunchArgs {
            auth_token: self.auth_token.clone(),
            machine_id: self.machine_id.clone(),
            machine_token: self.machine_token.clone(),
            main_exe_path: String::new(),
            main_exe_args: main_exe_args,
            watch_dog_path: String::new(),
            main_exe_version: String::new(),
        };

        Ok(args)
    }

    /// If needs to ask email address and fetch machine id
    pub fn needs_setup(&self) -> bool {
        self.email.is_empty() || self.machine_id.is_empty()
    }

    pub fn needs_password(&self) -> bool {
        self.auth_token.is_empty()
    }

    fn clear(&mut self) {
        self.machine_id = String::new();
        self.machine_token = String::new();
        self.email = String::new();
        self.auth_token = String::new();
    }

    /// Check if the machine id stored in the config match with the current machine
    /// returns true if the machine ids match
    pub fn check_machine_id(&mut self, guid: String) -> bool {
        let mut current_id = utils::machine_id(&guid);
        current_id.truncate(16);
        let result = self.machine_id == current_id;
        if !result {
            self.clear();
        }
        result
    }

    pub fn set_machine_id(&mut self, guid: String) {
        self.machine_id = utils::machine_id(&guid);
        self.machine_id.truncate(16);
    }

    #[allow(dead_code)] // TODO: should use this setter
    pub fn set_machine_token(&mut self, token: String) {
        self.machine_token = token;
    }

    pub fn set_email(&mut self, email: &str) {
        self.email = email.to_string();
    }

    pub fn get_email(&self) -> String {
        self.email.clone()
    }

    pub async fn check_update(&self, sku: String) -> Result<EdapiAssetMeta> {
        let resp = self
            .request
            .get(&format!("{}user/installer", API_URL_3))
            .query(&[
                ("machineToken", self.machine_token.clone()),
                ("authToken", self.auth_token.clone()),
                ("machineId", self.machine_id.clone()),
                ("sku", sku),
                ("os", "win".to_string()),
                ("fTime", utils::ftime()),
            ])
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let resp: EdapiAssetMeta = resp.json().await?;

        Ok(resp)
    }

    pub async fn fetch_manifest(&self, meta: &EdapiAssetMeta) -> Result<Vec<u8>> {
        let manifest_url = utils::ascii_decode(&meta.remote_path)?;
        let manifest_url = String::from_utf8_lossy(&manifest_url);
        let resp = self.request.get(manifest_url.as_ref()).send().await?;
        let mut resp = resp.error_for_status()?;
        let mut result = Vec::new();
        // ~10 MB
        result.reserve(10 * 1024 * 1024);
        let mut decompressor = GzipDecoder::new(result);
        while let Some(chunk) = resp.chunk().await? {
            decompressor.write_all(&chunk).await?;
        }
        decompressor.shutdown().await?;

        Ok(decompressor.into_inner())
    }

    pub async fn probe(&self) -> Result<()> {
        let resp: reqwest::Response = self
            .request
            .get(concat!(API_URL!(), "server/time"))
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let api_time: EdapiTime = resp.json().await?;
        let time_offset = utils::check_time(api_time.timestamp)?;
        if time_offset.abs() > 59 {
            let err = EdapiError::new(
                "The time on your device is probably incorrect".to_string(),
                Some(format!("Time difference in seconds: {}", time_offset)),
            );
            return Err(err.into());
        }
        let resp: reqwest::Response = self
            .request
            .get(concat!(API_URL!(), "client/version?version=", API_VER!()))
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let api_version: EdapiVersion = resp.json().await?;
        if api_version.version_status != "current" {
            let err = EdapiError::new(
                "API version mismatch".to_string(),
                Some(format!(
                    "Expected: {}, actual: {}",
                    api_version.version_latest, API_VERSION
                )),
            );
            return Err(err.into());
        }
        let resp: reqwest::Response = self.request.get(API_URL_STATUS).send().await?;
        let resp = resp.error_for_status()?;
        let api_status: EdapiStatus = resp.json().await?;
        if api_status.status != 2 {
            let err = EdapiError::new(
                "Server is currently unavailable".to_string(),
                Some(format!("{} ({})", api_status.text, api_status.status)),
            );
            return Err(err.into());
        }

        Ok(())
    }

    async fn auth_first_time<F: FnMut() -> String>(
        &mut self,
        user: String,
        secret: String,
        mut callback: F,
    ) -> Result<(), reqwest::Error> {
        let resp = self
            .request
            .post(concat!(API_URL!(), "user/auth"))
            .query(&[
                ("email", user),
                ("password", secret),
                ("machineId", self.machine_id.clone()),
                ("lang", "en".to_string()),
                ("fTime", utils::ftime()),
            ])
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let tokens: EdapiAuth = resp.json().await?;
        // verify using two factor code from email
        let plain_code: String = callback();
        let resp = self
            .request
            .post(concat!(API_URL!(), "user/token"))
            .query(&[
                ("machineId", self.machine_id.clone()),
                ("plainCode", plain_code),
                ("encCode", tokens.enc_code),
                ("lang", "en".to_string()),
            ])
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let tokens: EdapiAuth = resp.json().await?;
        // set the token in the struct
        self.machine_token = tokens.machine_token;

        Ok(())
    }

    pub async fn auth<F: FnMut() -> String>(
        &mut self,
        secret: &str,
        callback: F,
    ) -> Result<(), reqwest::Error> {
        let user = utils::ascii_encode(&self.email);
        let secret = utils::ascii_encode(secret);
        if self.machine_token.is_empty() {
            self.auth_first_time(user.clone(), secret.clone(), callback)
                .await?;
        }

        let resp = self
            .request
            .post(concat!(API_URL!(), "user/auth"))
            .query(&[
                ("email", user.clone()),
                ("password", secret),
                ("machineId", self.machine_id.clone()),
                ("machineToken", self.machine_token.clone()),
                ("lang", "en".to_string()),
                ("fTime", utils::ftime()),
            ])
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        let tokens: EdapiAuth = resp.json().await?;
        self.auth_token = tokens.auth_token;

        Ok(())
    }

    pub async fn get_product_list(&self) -> Result<Vec<EdapiProduct>, reqwest::Error> {
        let resp = self
            .request
            .get(&format!("{}user/purchases", API_URL_3))
            .bearer_auth(self.auth_token.clone())
            .query(&[("lang", "en"), ("ftime", &utils::ftime())])
            .send()
            .await?;
        let resp = resp.error_for_status()?;
        // * telemetry intensifies *
        self.log("ClientAuthorised").await.ok();
        self.log("AvailableProjects").await.ok();
        self.log("SupportedAddress").await.ok();
        // return product list
        let mut products: EdapiProducts = resp.json().await?;
        products
            .purchases
            .sort_unstable_by_key(|k| k.sortkey.clone());

        Ok(products.purchases)
    }

    pub async fn log(&self, event: &str) -> Result<(), reqwest::Error> {
        let resp = self
            .request
            .post(concat!(API_URL!(), "eventLog"))
            .query(&[
                ("eventTime", utils::ftime()),
                ("event", event.to_string()),
                ("machineToken", self.machine_token.clone()),
                ("authToken", self.auth_token.clone()),
                ("fTime", utils::ftime()),
                ("machineId", self.machine_id.clone()),
            ])
            .send()
            .await?;
        resp.error_for_status()?;

        Ok(())
    }
}

pub async fn download<P: AsRef<Path>>(url: &str, file: P) -> Result<u64> {
    let mut headers = HeaderMap::new();
    headers.insert(USER_AGENT, HeaderValue::from_static(API_USER_AGENT));
    let request = reqwest::Client::builder()
        .default_headers(headers)
        .build()?;
    let mut f = File::create(file).await?;
    let mut resp = request.get(url).send().await?;
    while let Some(chunk) = resp.chunk().await? {
        f.write_all(&chunk).await?;
    }
    Ok(resp.content_length().unwrap_or(0))
}

pub async fn download_installer<P: AsRef<Path>>(file: P) -> Result<()> {
    download(INSTALLER_URL, file).await?;
    Ok(())
}
